//////////////////////////////////////////////////////////////////////
//
// progress.hpp: Interface for progress indicator
// 
// Author: L. Abanes | September 2020
//
//
//////////////////////////////////////////////////////////////////////

#ifndef __PROGRESS_HPP
#define __PROGRESS_HPP

#include <mutex>
#include <iostream>


class Progress
{
private:
    float progress_ = 0.0f;
    std::mutex mutex_;
    size_t barWidth = 60;
    std::string fill_{"#"}, remainder_{" "}, statusText{""};

public:
    Progress();
    ~Progress() {}

    void setProgress(float value);
    void setBarWidth(size_t width);
    void setStatusText(const std::string& status);
    void writeProgress(std::ostream& os = std::cout);
    void fillBarProgressWith(const std::string& chars);
    void fillBarRemainderWith(const std::string& chars);
    void update(float value, std::ostream& os = std::cout);    
};

#endif
