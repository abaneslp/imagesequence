// progress.cpp: Implementation for progress indicator
// 
//////////////////////////////////////////////////////////////////////

#include "progress.hpp"

#include <atomic>
#include <mutex>
#include <iostream>

Progress::Progress()
{
    setBarWidth(50);
    fillBarProgressWith("■");
    fillBarRemainderWith(" ");
}


void Progress::setProgress(float value)
{
    std::unique_lock lock(mutex_);
    progress_ = value;
}

void Progress::setBarWidth(size_t width)
{
    std::unique_lock lock(mutex_);
    barWidth = width;
}

void Progress::setStatusText(const std::string& status)
{
    std::unique_lock lock(mutex_);
    statusText = status;
}

void Progress::writeProgress(std::ostream& os)
{
    std::unique_lock lock(mutex_);
    if (progress_ > 100.0f) return;
    os << "\r" << std::flush;
    os << "[";
    const auto completed = static_cast<size_t>(progress_ * static_cast<float>(barWidth)/100.0);
    for (size_t i=0; i<barWidth; ++i)
    {
        if (i <= completed) os << fill_;
        else os << remainder_;
    }
    os << "]";
    os << " " << std::min(static_cast<size_t>(progress_), static_cast<size_t>(100)) << "%";
    os << " " << statusText;
}

void Progress::fillBarProgressWith(const std::string& chars)
{
    std::unique_lock lock{mutex_};
    fill_ = chars;
}


void Progress::fillBarRemainderWith(const std::string& chars)
{
    std::unique_lock lock{mutex_};
    remainder_ = chars;
}


void Progress::update(float value, std::ostream& os)
{
    setProgress(value);
    writeProgress(os);
}
